//
//  TableViewCell.swift
//  Pokedex cauezin
//
//  Created by COTEMIG on 02/06/22.
//

import UIKit

class TableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var typeFirst: UILabel!
    @IBOutlet weak var typeSecond: UILabel!
    @IBOutlet weak var img: UIImageView!
    
}

