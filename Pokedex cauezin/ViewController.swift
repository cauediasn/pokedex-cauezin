//
//  ViewController.swift
//  Pokedex cauezin
//
//  Created by COTEMIG on 24/02/22.
//

import UIKit

class ListaPokemonViewController: UIViewController, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pokemons.count
    }
    
    private var pokemons = [Pokemon]()
    
    
    
    @IBOutlet weak var tableView: UITableView!
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if let cell = tableView.dequeueReusableCell(withIdentifier: "celula") as? TableViewCell{
            cell.name.text = pokemons[indexPath.row].nome
         
            if pokemons[indexPath.row].typeFirst != nil{
                cell.typeFirst.text = pokemons[indexPath.row].typeFirst
            }else{
            }
            if pokemons[indexPath.row].typeSecond != nil{
                cell.typeSecond.text = pokemons[indexPath.row].typeSecond
            }else{
            }
            cell.img.image = UIImage(named: pokemons[indexPath.row].img)
            cell.contentView.backgroundColor = hexStringToUIColor(hex: pokemons[indexPath.row].color)
            return cell
        }
        else{
            fatalError("error")
        }
    }
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        tableView.dataSource = self
        pokemons = [
            Pokemon(nome: "Bulbassauro", typeFirst: "Grama", typeSecond: "Veneno", img: "bulbassauro", color: "#48D0B0"),
            Pokemon(nome: "Ivyssauro", typeFirst: "Grama", typeSecond: "Veneno", img: "ivissauro", color: "#48D0B0"),
            Pokemon(nome: "Venussauro", typeFirst: "Grama", typeSecond: "Veneno", img: "venussauro", color: "#48D0B0"),
            Pokemon(nome: "Charmander", typeFirst: "Fogo", typeSecond: nil, img: "charmander", color: "#FB6C6C"),
            Pokemon(nome: "Charmeleon", typeFirst: "Fogo", typeSecond: nil, img: "charmeleon", color: "#FB6C6C"),
            Pokemon(nome: "Charizard", typeFirst: "Fogo", typeSecond: "Voador", img: "charizard", color: "#FB6C6C"),
            Pokemon(nome: "Squirtle", typeFirst: "Agua", typeSecond: nil, img: "squirtle", color: "#77BDFE"),
            Pokemon(nome: "Wartotle", typeFirst: "Agua", typeSecond: nil, img: "wartotle", color: "#77BDFE"),
            Pokemon(nome: "Blastoise", typeFirst: "Agua", typeSecond: nil, img: "blastoise", color: "#77BDFE"),
            Pokemon(nome: "Pikachu", typeFirst: "Eletrico", typeSecond: nil, img: "pikachu", color: "#FFCE4B"),
        ]
    }
    struct Pokemon{
        var nome: String
        var typeFirst: String?
        var typeSecond: String?
        var img: String
        var color: String
    }
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
